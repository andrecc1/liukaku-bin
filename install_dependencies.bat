echo "start install -->"

cd ..
cd liukaku-common
call mvn clean package install

cd ..
cd liukaku-common-domain
call mvn clean package install

cd ..
cd liukaku-common-service
call mvn clean package install

cd ..
cd liukaku-common-web
call mvn clean install

cd ..
cd liukaku-dependencies
call mvn clean install

cd ..
echo " Jobs done -->"