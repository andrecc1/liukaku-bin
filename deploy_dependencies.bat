echo "start deploy -->"

cd ..
cd liukaku-common
call mvn clean package deploy

cd ..
cd liukaku-common-domain
call mvn clean package deploy

cd ..
cd liukaku-common-service
call mvn clean package deploy

cd ..
cd liukaku-common-web
call mvn clean deploy

cd ..
cd liukaku-dependencies
call mvn clean deploy

cd ..
cd liukaku-service-redis
call mvn clean deploy

cd ..
cd liukaku-service-sso
call mvn clean deploy

cd ..
cd liukaku-service-admin-tw
call mvn clean deploy

cd ..
cd liukaku-web-admin-tw
call mvn clean deploy

cd ..
cd liukaku-service-posts
call mvn clean deploy

cd ..
echo " Jobs done -->"